package cz.uhk.umte_customeventplanner;

import org.junit.Test;

import java.util.ArrayList;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        final ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        System.out.println("Integer na pozici 1: " + integers.get(1));
        integers.remove(0);
        System.out.println("Integer na pozici 1 po vymazání pozice 0: " + integers.get(1));
        String testString = "Slepice";
        testString += "Kohout" + " ";
        testString += "Pes";
        System.out.println(testString);
        double doubleTest = 1.111111111111;
        doubleTest /= 1.11;
        System.out.println(doubleTest);

        int a = 1;
        int b = 2;
        a += b;
        System.out.println(a);
        final String empty = "";
        System.out.println(empty.isEmpty());
    }
}