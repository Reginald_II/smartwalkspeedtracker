package cz.uhk.umte_customeventplanner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.util.List;

import cz.uhk.umte_customeventplanner.model.Event;
import cz.uhk.umte_customeventplanner.R;
import cz.uhk.umte_customeventplanner.utility.time.TimeUtility;

/**
 * Adapter of the recycle view, used for showing the list of events.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final List<Event> events;

    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(final View itemView, final int position);
    }

    public void setOnItemClickListener(final OnItemClickListener listener) {
        this.listener = listener;
    }

    /**
     * Initializes events.
     *
     * @param events list of events.
     */
    public RecyclerViewAdapter(final List<Event> events) {
        this.events = events;
    }

    /**
     * Sets up the view holder used by the adapter.
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView name = itemView.findViewById(R.id.event_Name);
        private final TextView timeRemaining = itemView.findViewById(R.id.timeRemainingText);

        public ViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(final View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(itemView, position);
            }
        }
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final Context context = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View contactView = inflater.inflate(R.layout.recycle_view_layout, parent, false);
        final ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    /**
     * Fills the output text of the view holder..
     *
     * @param holder   View holder of the adapter.
     * @param position Position in the list.
     */
    @Override
    public void onBindViewHolder(final RecyclerViewAdapter.ViewHolder holder, final int position) {
        try {
            final String remainingTime = TimeUtility.calculateTimeUntilEventHappens(events.get(position));
            if (remainingTime != null && !remainingTime.isEmpty()) {
                final TextView timeRemainingView = holder.timeRemaining;
                timeRemainingView.setText(remainingTime);
            }
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        final TextView textView = holder.name;
        textView.setText(events.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return events.size();
    }
}
