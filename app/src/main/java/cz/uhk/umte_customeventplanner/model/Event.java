package cz.uhk.umte_customeventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Saves the variables of an event.
 */
public class Event implements Parcelable {

    private String name, description, locationFrom, location, dateOfEvent, timeOfBeginning;
    private byte isDaily;
    //Distance in meters between locationFrom and location places.
    private float distanceBetweenLocations;

    public Event(final String name, final String description, final String locationFrom, final String location, final String dateOfEvent, final String timeOfBeginning, final byte isDaily) {
        this.name = name;
        this.description = description;
        this.locationFrom = locationFrom;
        this.location = location;
        this.dateOfEvent = dateOfEvent;
        this.timeOfBeginning = timeOfBeginning;
        this.isDaily = isDaily;
        this.distanceBetweenLocations = 0.00f;
    }

    /**
     * Loads the variables from the parcel where was the instance saved.
     *
     * @param in Parcel which has the data of the instance of the class.
     */
    protected Event(final Parcel in) {
        name = in.readString();
        description = in.readString();
        locationFrom = in.readString();
        location = in.readString();
        dateOfEvent = in.readString();
        timeOfBeginning = in.readString();
        isDaily = in.readByte();
        distanceBetweenLocations = in.readFloat();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public String getDateOfEvent() {
        return dateOfEvent;
    }

    public void setDateOfEvent(final String dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }

    public String getTimeOfBeginning() {
        return timeOfBeginning;
    }

    public void setTimeOfBeginning(final String timeOfBeginning) {
        this.timeOfBeginning = timeOfBeginning;
    }

    public String getLocationFrom() {
        return locationFrom;
    }

    public void setLocationFrom(String locationFrom) {
        this.locationFrom = locationFrom;
    }

    public byte getIsDaily() {
        return isDaily;
    }

    public void setIsDaily(byte isDaily) {
        this.isDaily = isDaily;
    }

    public double getDistanceBetweenLocations() {
        return distanceBetweenLocations;
    }

    public void setDistanceBetweenLocations(final float distanceBetweenLocations) {
        this.distanceBetweenLocations = distanceBetweenLocations;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Saves variables to the destination Parcel.
     *
     * @param dest  Destination parcel.
     * @param flags
     */
    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(location);
        dest.writeString(dateOfEvent);
        dest.writeString(timeOfBeginning);
        dest.writeString(locationFrom);
        dest.writeByte(isDaily);
        dest.writeFloat(distanceBetweenLocations);
    }
}
