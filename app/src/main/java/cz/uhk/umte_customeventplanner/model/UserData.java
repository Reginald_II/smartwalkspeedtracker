package cz.uhk.umte_customeventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserData implements Parcelable {
    //Walking speed of the user in m/s^2.
    private double walkingSpeed;


    public UserData(final double walkingSpeed) {
        this.walkingSpeed = walkingSpeed;
    }

    protected UserData(final Parcel in) {
        walkingSpeed = in.readDouble();
    }

    @Override
    public void writeToParcel(final Parcel dest, int flags) {
        dest.writeDouble(walkingSpeed);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserData> CREATOR = new Creator<UserData>() {
        @Override
        public UserData createFromParcel(final Parcel in) {
            return new UserData(in);
        }

        @Override
        public UserData[] newArray(final int size) {
            return new UserData[size];
        }
    };

    public double getWalkingSpeed() {
        return walkingSpeed;
    }

    public void setWalkingSpeed(final double walkingSpeed) {
        this.walkingSpeed = walkingSpeed;
    }


}
