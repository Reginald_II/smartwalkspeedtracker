package cz.uhk.umte_customeventplanner.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import cz.uhk.umte_customeventplanner.R;

/**
 * Fragment used for a date picker dialog for picking up a date by a user.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private TextView dateInput;

    /**
     * Sets current system date as the default value in the date picker dialog. Or fills the values by the existing ones in the input.
     *
     * @param savedInstanceState
     * @return a date picker dialog
     */
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        if (dateInput == null) {
            dateInput = getActivity().findViewById(R.id.dateInput);
        }
        final String dateString = dateInput.getText().toString();
        final int year;
        int month;
        final int day;
        if (dateString != null && !dateString.isEmpty()) {
            final String[] splitString = dateString.split("\\.");
            day = Integer.parseInt(splitString[0]);
            month = Integer.parseInt(splitString[1]);
            //Fix for the month being one more than the one selected.
            if (month > 1) {
                month--;
            } else {
                month = 12;
            }
            year = Integer.parseInt(splitString[2]);
        } else {
            final Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    /**
     * Saves the chosen date by user into a string in dd/mm/yyyy format and then sets the date input of the new event of it.
     *
     * @param view
     * @param year
     * @param month
     * @param dayOfMonth
     */
    @Override
    public void onDateSet(final DatePicker view, final int year, final int month, final int dayOfMonth) {
        //For some reason the Date picker picks one less month than the real one, which is chosen by the user. The problem is fixed by adding the missing one to the month variable.
        int fixedMonth = month + 1;
        while (fixedMonth > 12) {
            fixedMonth -= 12;
        }
        final String date = dayOfMonth + "." + fixedMonth + "." + year;
        final TextView dateInput = getActivity().findViewById(R.id.dateInput);
        dateInput.setText(date);
    }
}
