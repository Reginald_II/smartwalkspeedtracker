package cz.uhk.umte_customeventplanner.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import cz.uhk.umte_customeventplanner.R;
import cz.uhk.umte_customeventplanner.model.Event;

/**
 * Shows GUI to user for changing an event.
 */
public class ChangeEventFragment extends Fragment {

    private Event event;
    private int eventPosition;

    @Override
    public View onCreateView(
            final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_event, container, false);
    }

    public void onViewCreated(@NonNull final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        event = this.getArguments().getParcelable("eventToChange");
        eventPosition = this.getArguments().getInt("eventPosition");
        fillInput();
        view.findViewById(R.id.button_cancelChange).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("detailedEvent", event);
                bundle.putInt("eventPosition", eventPosition);
                NavHostFragment.findNavController(ChangeEventFragment.this).navigate(R.id.action_changeEventFragment_to_detailedEventFragment, bundle);
            }
        });
        view.findViewById(R.id.button_change_event).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final boolean eventChanged = changeEvent();
                //In case of null event being returned the method ends and thus makes the user to stay on the screen to fill the missing input.
                if (!eventChanged) {
                    return;
                }
                final Bundle bundle = new Bundle();
                bundle.putParcelable("changeEvent", event);
                bundle.putInt("eventPosition", eventPosition);
                NavHostFragment.findNavController(ChangeEventFragment.this).navigate(R.id.action_changeEventFragment_to_FirstFragment, bundle);
            }
        });
        view.findViewById(R.id.dateInput).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final DialogFragment fragment = new DatePickerFragment();
                fragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });
        view.findViewById(R.id.startTimeInput).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final DialogFragment fragment = new TimePickerFragment();
                fragment.show(getActivity().getSupportFragmentManager(), "timePicker");
            }
        });
    }

    /**
     * Fills input by the existing data from the event.
     */
    private void fillInput() {
        final TextView nameInput = getActivity().findViewById(R.id.nameInput);
        nameInput.setText(event.getName());

        final TextView descriptionInput = getActivity().findViewById(R.id.descriptionInput);
        descriptionInput.setText(event.getDescription());

        final TextView placeFromInput = getActivity().findViewById(R.id.locationFromInput);
        placeFromInput.setText(event.getLocationFrom());

        final TextView placeInput = getActivity().findViewById(R.id.locationInput);
        placeInput.setText(event.getLocation());

        final TextView dateInput = getActivity().findViewById(R.id.dateInput);
        dateInput.setText(event.getDateOfEvent());

        final TextView timeStartInput = getActivity().findViewById(R.id.startTimeInput);
        timeStartInput.setText(event.getTimeOfBeginning());

        final Switch isDailySwitch = getActivity().findViewById(R.id.isDailyEventSwitch);
        final boolean isDaily;
        if (event.getIsDaily() < 1) {
            isDailySwitch.setChecked(false);
        } else {
            isDailySwitch.setChecked(true);
        }
    }

    /**
     * Saves the received input from the user into a single Event object.
     *
     * @return An instance of Event with the user´s input or null if user didn´t fill any input.
     */
    private boolean changeEvent() {
        final TextView nameInput = getActivity().findViewById(R.id.nameInput);
        final String name = nameInput.getText().toString();

        if (name == null || name.isEmpty()) {
            Snackbar.make(getView(), "Musíte vyplnit název události.", BaseTransientBottomBar.LENGTH_SHORT).show();
            return false;
        }
        if (!event.getName().equals(name)) {
            event.setName(name);
        }

        final TextView descriptionInput = getActivity().findViewById(R.id.descriptionInput);
        final String description = descriptionInput.getText().toString();

        if (!event.getDescription().equals(description)) {
            event.setDescription(description);
        }

        final TextView placeFromInput = getActivity().findViewById(R.id.locationFromInput);
        final String placeFrom = placeFromInput.getText().toString();

        if (!event.getLocationFrom().equals(placeFrom)) {
            event.setLocationFrom(placeFrom);
        }

        final TextView placeInput = getActivity().findViewById(R.id.locationInput);
        final String place = placeInput.getText().toString();

        if (place == null || place.isEmpty()) {
            Snackbar.make(getView(), "Musíte vyplnit místo, kam jdete.", BaseTransientBottomBar.LENGTH_SHORT).show();
            return false;
        }

        if (!place.equals(event.getLocation())) {
            event.setLocation(place);
        }

        final TextView dateInput = getActivity().findViewById(R.id.dateInput);
        final String dateText = dateInput.getText().toString();

        if (dateText == null || dateText.isEmpty()) {
            Snackbar.make(getView(), "Musíte vyplnit dobu události.", BaseTransientBottomBar.LENGTH_SHORT).show();
            return false;
        }
        if (!event.getDateOfEvent().equals(dateText)) {
            event.setDateOfEvent(dateText);
        }

        final TextView timeStartInput = getActivity().findViewById(R.id.startTimeInput);
        final String startTimeText = timeStartInput.getText().toString();

        if (startTimeText == null || startTimeText.isEmpty()) {
            Snackbar.make(getView(), "Musíte vyplnit čas začátku události.", BaseTransientBottomBar.LENGTH_SHORT).show();
            return false;
        }

        if (!event.getTimeOfBeginning().equals(startTimeText)) {
            event.setTimeOfBeginning(startTimeText);
        }

        final Switch isDailyInput = getActivity().findViewById(R.id.isDailyEventSwitch);
        final boolean isDaily = isDailyInput.isChecked();
        if (!isDaily && event.getIsDaily() != 0) {
            event.setIsDaily((byte) 0);
        }
        if (isDaily && event.getIsDaily() != 1) {
            event.setIsDaily((byte) 1);
        }
        return true;
    }
}