package cz.uhk.umte_customeventplanner.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cz.uhk.umte_customeventplanner.R;
import cz.uhk.umte_customeventplanner.model.Event;
import cz.uhk.umte_customeventplanner.model.UserData;
import cz.uhk.umte_customeventplanner.utility.time.TimeUtility;
import cz.uhk.umte_customeventplanner.utility.save.list.EventSaveUtility;
import cz.uhk.umte_customeventplanner.utility.save.object.UserDataSaveUtility;

/**
 * Fragment working with sensors and showing their output to the user.
 */
public class EventSensorsFragment extends Fragment implements LocationListener, SensorEventListener {

    private Event event;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private final List<Double> totalSpeeds = new ArrayList<>();
    private Timer accelerometerTimer = new Timer();
    private TextView averageSpeedText, positionStreet, distanceText;
    //Boolean for checking if the average speed output should be updated. The default value is true.
    private boolean showAverageSpeed = true;
    private Location positionLocation, destinationLocation;
    private float distanceToDestination = 0.00f;
    private LocationManager locationManager;
    private UserDataSaveUtility userDataSaveUtility;

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        event = this.getArguments().getParcelable("detailedEvent");
        userDataSaveUtility = new UserDataSaveUtility(getActivity().getPreferences(Context.MODE_PRIVATE));
        final TextView eventNameText = view.findViewById(R.id.eventMeasureName);
        eventNameText.setText("Název události: " + event.getName());
        positionLocation = getUserLocationData();
        destinationLocation = getDestinationLocationData();
        if (destinationLocation.getLongitude() > 0 && destinationLocation.getLatitude() > 0) {
            distanceToDestination = positionLocation.distanceTo(destinationLocation);
            distanceText = view.findViewById(R.id.distanceText);
            distanceText.setText("Vzdálenost v m: " + distanceToDestination);
            System.out.println("Distance to destination is: " + distanceToDestination);
        }
        averageSpeedText = getView().findViewById(R.id.totalSpeedText);
        setUpAccelerometer();

        view.findViewById(R.id.button_backToDetailedEvent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("detailedEvent", event);
                NavHostFragment.findNavController(EventSensorsFragment.this).navigate(R.id.action_eventSensorsFragment_to_detailedEventFragment, bundle);
            }
        });
        final EventSaveUtility eventSaveUtility = new EventSaveUtility(getActivity().getPreferences(Context.MODE_PRIVATE));
        if (event.getDistanceBetweenLocations() <= 0.00 && event.getLocationFrom() != null && !event.getLocationFrom().isEmpty()) {
            final Location locationFrom = getLocationByName(event.getLocationFrom());
            final float distance = locationFrom.distanceTo(destinationLocation);
            if (event.getDistanceBetweenLocations() != distance) {
                event.setDistanceBetweenLocations(distance);
                eventSaveUtility.change(event, this.getArguments().getInt("eventPosition"));
            }
        }
    }

    /**
     * Gets the user location data by the GPS sensor and fills its fields in the view, if it succeeds.
     */
    private Location getUserLocationData() {
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1000, this);
        final Location userLocation = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
        final Geocoder geocoder = new Geocoder(getContext());
        try {
            final List<Address> adresses = geocoder.getFromLocation(userLocation.getLatitude(), userLocation.getLongitude(), 1);
            if (adresses != null && adresses.size() > 0) {
                final Address userLocationAddress = adresses.get(0);
                if (userLocationAddress.getMaxAddressLineIndex() > -1) {
                    String address = "";
                    for (int i = 0; i <= userLocationAddress.getMaxAddressLineIndex(); i++) {
                        address += userLocationAddress.getAddressLine(i) + " ";
                    }
                    positionStreet = getView().findViewById(R.id.addressOfPositionText);
                    positionStreet.setText("Adresa pozice: " + address);
                }
            }
        } catch (final IOException e) {
            Snackbar.make(getView(), "Během získávání údajů Vaší polohy došlo k chybě.", BaseTransientBottomBar.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return userLocation;
    }

    /**
     * Gets the data of the destination by the GPS sensor and fills the destination text views if no error occurs.
     */
    private Location getDestinationLocationData() {
        final Location destinationLocation = new Location(LocationManager.GPS_PROVIDER);
        try {
            final Geocoder geocoder = new Geocoder(getContext());
            final List<Address> addresses = geocoder.getFromLocationName(event.getLocation(), 1);
            if (addresses != null && addresses.size() > 0) {
                final Address address = addresses.get(0);
                if (address.hasLatitude() && address.hasLongitude()) {
                    destinationLocation.setLatitude(address.getLatitude());
                    destinationLocation.setLongitude(address.getLongitude());
                }
                if (address.getMaxAddressLineIndex() > -1) {
                    String destinationStreet = "";
                    for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                        destinationStreet += address.getAddressLine(i) + " ";
                    }
                    final TextView destinationStreetText = getView().findViewById(R.id.addressOfDestination);
                    destinationStreetText.setText("Adresa cíle: " + destinationStreet);
                }
            }
        } catch (final IOException e) {
            Snackbar.make(getView(), "Během získávání údajů polohy pozice události došlo k chybě.", BaseTransientBottomBar.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return destinationLocation;
    }

    private Location getLocationByName(final String name) {
        final Location location = new Location(LocationManager.GPS_PROVIDER);
        try {
            final Geocoder geocoder = new Geocoder(getContext());
            final List<Address> addresses = geocoder.getFromLocationName(name, 1);
            if (addresses != null && addresses.size() > 0) {
                final Address address = addresses.get(0);
                if (address.hasLatitude() && address.hasLongitude()) {
                    location.setLatitude(address.getLatitude());
                    location.setLongitude(address.getLongitude());
                }
            }
        } catch (final IOException e) {
            Snackbar.make(getView(), "Během získávání údajů polohy pozice z místa vycházení došlo k chybě.", BaseTransientBottomBar.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return location;
    }

    /**
     * Sets up the accelerometer, its listener and timer.
     */
    private void setUpAccelerometer() {
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        accelerometerTimer.schedule(initializeAverageSpeedTimer(), 10000, 10000);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event_sensors, container, false);
    }

    /**
     * Listener being triggered after either the set amount of time or distance passess. Gets new information about the current user´s location by the GPS sensor and updates the output.
     *
     * @param location
     */
    @Override
    public void onLocationChanged(@NonNull final Location location) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        positionLocation = location;
        final Geocoder geocoder = new Geocoder(getContext());
        try {
            final List<Address> addresses = geocoder.getFromLocation(positionLocation.getLatitude(), positionLocation.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                final Address userLocationAddress = addresses.get(0);
                if (userLocationAddress.getMaxAddressLineIndex() > -1) {
                    String address = "";
                    for (int i = 0; i <= userLocationAddress.getMaxAddressLineIndex(); i++) {
                        address += userLocationAddress.getAddressLine(i) + " ";
                    }
                    if (positionStreet == null) {
                        positionStreet = getView().findViewById(R.id.addressOfPositionText);
                    }
                    positionStreet.setText("Adresa pozice: " + address);
                }
            }
        } catch (final IOException e) {
            Snackbar.make(getView(), "Během aktualizace údajů Vaší polohy došlo k chybě.", BaseTransientBottomBar.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        if (destinationLocation != null && destinationLocation.getLongitude() > 0 && destinationLocation.getLatitude() > 0) {
            distanceToDestination = positionLocation.distanceTo(destinationLocation);
            if (distanceText == null) {
                distanceText = getView().findViewById(R.id.distanceText);
            }
            distanceText.setText("Vzdálenost v m: " + distanceToDestination);
            System.out.println("Distance to destination is: " + distanceToDestination);
        }
    }

    @Override
    public void onStatusChanged(final String provider, final int status, final Bundle extras) {

    }

    @Override
    public void onProviderDisabled(@NonNull final String provider) {

    }

    @Override
    public void onProviderEnabled(@NonNull final String provider) {

    }

    /**
     * Method which is used by the listener of the accelometer sensor and gets called when this sensor get new values. The method works with the data obtained by the sensor and shows them to the user.
     *
     * @param event A variable with the new data of a sensor.
     */
    @Override
    public void onSensorChanged(final SensorEvent event) {
        if (event.sensor == accelerometer) {
            final float[] values = event.values;
            final double totalSpeed = Math.sqrt(Math.pow(values[0], 2) + Math.pow(values[1], 2) + Math.pow(values[2], 2));
            totalSpeeds.add(totalSpeed);
            //Checks if the average speed update is allowed by the boolean.
            if (showAverageSpeed) {
                double totalSpeedSum = 0.00;
                for (double totalSpeedVariable : totalSpeeds) {
                    totalSpeedSum += totalSpeedVariable;
                }
                final double averageTotalSpeed = totalSpeedSum / totalSpeeds.size();
                if (averageTotalSpeed > 0.5 && userDataSaveUtility.getUserData().getWalkingSpeed() != averageTotalSpeed) {
                    final UserData userData = userDataSaveUtility.getUserData();
                    userData.setWalkingSpeed(averageTotalSpeed);
                    userDataSaveUtility.setUserData(userData);
                }
                averageSpeedText.setText("Průměrná rychlost v m/s: " + averageTotalSpeed);
                if (distanceToDestination > 0 && averageTotalSpeed > 0) {
                    //Based on the other values, calculated timeOfTravel is in seconds.
                    double timeOfTravel = distanceToDestination / averageTotalSpeed;
                    final TextView adviceText = getView().findViewById(R.id.walkingSpeedInfoText);
                    try {
                        final String adviceForUser = TimeUtility.calculateIfUserCanMakeItInTime(this.event, timeOfTravel);
                        adviceText.setText(adviceForUser);
                    } catch (final ParseException e) {
                        adviceText.setText("Z důvodu chyby nelze zjistit, zda stihnete dojít včas na dané místo.");
                        e.printStackTrace();
                    }
                    //Converting the time of travel from seconds to hours.
                    timeOfTravel = (timeOfTravel / 60) / 60;
                    final String timeOfTravelString = Double.toString(timeOfTravel);
                    final String[] dividedHoursAndMinutes = timeOfTravelString.split("\\.");
                    final String hour = dividedHoursAndMinutes[0];
                    final String minutesInDecimal = "0." + dividedHoursAndMinutes[1];
                    final double minutes = Double.parseDouble(minutesInDecimal) * 60;
                    final TextView timeText = getView().findViewById(R.id.timeUntilArrival);
                    timeText.setText("Doba cesty: " + hour + " hodin a " + Math.round(minutes) + " minut");
                }
                //Deletes the saved speeds and disables the updating of average speed by setting false the showAverageSpeed boolean.
                totalSpeeds.clear();
                showAverageSpeed = false;
            }
        }
    }

    /**
     * Initializes the timer task which enables updating the average speed output by the accelometer.
     *
     * @return
     */
    private TimerTask initializeAverageSpeedTimer() {
        final TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                showAverageSpeed = true;
            }
        };
        return timerTask;
    }

    @Override
    public void onAccuracyChanged(final Sensor sensor, final int accuracy) {

    }

    /**
     * Stops the sensors by disabling their listeners and a timer when the fragment is paused.
     */
    @Override
    public void onPause() {
        super.onPause();
        accelerometerTimer.cancel();
        locationManager.removeUpdates(this);
        sensorManager.unregisterListener(this);
    }

    /**
     * Restores the sensors by re-enabling their listeners and a timer when the fragment is resumed.
     */
    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        accelerometerTimer = new Timer();
        accelerometerTimer.schedule(initializeAverageSpeedTimer(), 10000, 10000);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1000, this);
    }

    /**
     * On exiting the fragment, disables the sensors by disabling their listeners and a timer in order to stop them from using the no-longer existing view variables.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        accelerometerTimer.cancel();
        sensorManager.unregisterListener(this);
        locationManager.removeUpdates(this);
    }
}