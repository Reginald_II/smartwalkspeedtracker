package cz.uhk.umte_customeventplanner.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import cz.uhk.umte_customeventplanner.R;
import cz.uhk.umte_customeventplanner.model.Event;

/**
 * Shows GUI to user for adding a new event.
 */
public class AddEventFragment extends Fragment {

    @Override
    public View onCreateView(
            final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_event, container, false);
    }

    public void onViewCreated(@NonNull final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /**
         * On pressing cancel button returns user back to the list of events.
         */
        view.findViewById(R.id.button_cancelChange).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                NavHostFragment.findNavController(AddEventFragment.this).navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
        view.findViewById(R.id.button_change_event).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Event event = saveEvent();
                //In case of null event being returned the method ends and thus makes the user to stay on the screen to fill the missing input.
                if (event == null) {
                    return;
                }
                final Bundle bundle = new Bundle();
                bundle.putParcelable("event", event);
                NavHostFragment.findNavController(AddEventFragment.this).navigate(R.id.action_SecondFragment_to_FirstFragment, bundle);
            }
        });
        view.findViewById(R.id.dateInput).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final DialogFragment fragment = new DatePickerFragment();
                fragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });
        view.findViewById(R.id.startTimeInput).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final DialogFragment fragment = new TimePickerFragment();
                fragment.show(getActivity().getSupportFragmentManager(), "timePicker");
            }
        });
    }

    /**
     * Checks if user filled all inputs. Then turns all the received inputs from the user into a single Event class, which is then returned.
     *
     * @return Event containing the user´s input. Or null if there is any input missing.
     */
    private Event saveEvent() {
        final TextView nameInput = getActivity().findViewById(R.id.nameInput);
        final String name = nameInput.getText().toString();

        if (name == null || name.isEmpty()) {
            Snackbar.make(getView(), "Musíte vyplnit název události.", BaseTransientBottomBar.LENGTH_SHORT).show();
            return null;
        }

        final TextView descriptionInput = getActivity().findViewById(R.id.descriptionInput);
        final String description = descriptionInput.getText().toString();

        final TextView placeFromInput = getActivity().findViewById(R.id.locationFromInput);
        final String placeFrom = placeFromInput.getText().toString();

        final TextView placeInput = getActivity().findViewById(R.id.locationInput);
        final String place = placeInput.getText().toString();

        if (place == null || place.isEmpty()) {
            Snackbar.make(getView(), "Musíte vyplnit místo, kam jdete.", BaseTransientBottomBar.LENGTH_SHORT).show();
            return null;
        }

        final TextView dateInput = getActivity().findViewById(R.id.dateInput);
        final String dateText = dateInput.getText().toString();

        if (dateText == null || dateText.isEmpty()) {
            Snackbar.make(getView(), "Musíte vyplnit dobu události.", BaseTransientBottomBar.LENGTH_SHORT).show();
            return null;
        }
        final TextView timeStartInput = getActivity().findViewById(R.id.startTimeInput);
        final String startTimeText = timeStartInput.getText().toString();

        if (startTimeText == null || startTimeText.isEmpty()) {
            Snackbar.make(getView(), "Musíte vyplnit čas začátku události.", BaseTransientBottomBar.LENGTH_SHORT).show();
            return null;
        }
        final Switch isDailyInput = getActivity().findViewById(R.id.isDailyEventSwitch);
        final boolean isDaily = isDailyInput.isChecked();

        final byte isDailyByte;
        if (!isDaily) {
            isDailyByte = 0;
        } else {
            isDailyByte = 1;
        }

        final Event event = new Event(name, description, placeFrom, place, dateText, startTimeText, isDailyByte);
        return event;

    }
}