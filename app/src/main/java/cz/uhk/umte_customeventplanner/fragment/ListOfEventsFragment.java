package cz.uhk.umte_customeventplanner.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import cz.uhk.umte_customeventplanner.R;
import cz.uhk.umte_customeventplanner.adapter.RecyclerViewAdapter;
import cz.uhk.umte_customeventplanner.model.Event;
import cz.uhk.umte_customeventplanner.utility.save.list.EventSaveUtility;

/**
 * Main fragment of the app, showing the list of saving events and also is used to access the functions of the app.
 */
public class ListOfEventsFragment extends Fragment {

    private List<Event> events;

    @Override
    public View onCreateView(
            final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_of_events, container, false);
    }

    /**
     * First works with the data of events according to the incoming Parcels and either changes them, saves them or deletes them. Then sets up the on click listeners and the list of events.
     *
     * @param view               View of the fragment.
     * @param savedInstanceState
     */
    public void onViewCreated(@NonNull final View view, final Bundle savedInstanceState) {
        final EventSaveUtility utility = new EventSaveUtility(getActivity().getPreferences(Context.MODE_PRIVATE));
        //Gets arriving bundle arguments and checks if there is an event object incoming.
        events = utility.getEvents();

        if (this.getArguments().getParcelable("event") != null) {
            final Event event = this.getArguments().getParcelable("event");
            events = utility.add(event);
            Snackbar.make(view, "Událost " + event.getName() + " byla úspěšně uložena.", BaseTransientBottomBar.LENGTH_SHORT).show();
        }
        if (this.getArguments().getParcelable("changeEvent") != null) {
            final Event event = this.getArguments().getParcelable("changeEvent");
            final int eventPosition = this.getArguments().getInt("eventPosition");
            events = utility.change(event, eventPosition);
            Snackbar.make(view, "Událost " + event.getName() + " byla úspěšně změněna.", BaseTransientBottomBar.LENGTH_SHORT).show();
        }
        if (this.getArguments().getBoolean("delete")) {
            final int deleteId = this.getArguments().getInt("deleteId");
            final String name = events.get(deleteId).getName();
            events = utility.remove(deleteId);
            Snackbar.make(view, "Událost " + name + " byla úspěšně snazána.", BaseTransientBottomBar.LENGTH_SHORT).show();
        }
        this.getArguments().clear();
        view.findViewById(R.id.ButtonAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                NavHostFragment.findNavController(ListOfEventsFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
        final RecyclerView rvEvents = getActivity().findViewById(R.id.listOfItems);
        final RecyclerViewAdapter recycleViewAdapter = new RecyclerViewAdapter(events);
        recycleViewAdapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                final Event event = events.get(position);
                final Bundle bundle = new Bundle();
                bundle.putParcelable("detailedEvent", event);
                bundle.putInt("eventPosition", position);
                NavHostFragment.findNavController(ListOfEventsFragment.this).navigate(R.id.action_showDetailedEvent, bundle);
            }
        });
        rvEvents.setAdapter(recycleViewAdapter);
        rvEvents.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}