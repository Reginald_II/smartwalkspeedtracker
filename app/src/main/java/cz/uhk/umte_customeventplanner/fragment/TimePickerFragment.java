package cz.uhk.umte_customeventplanner.fragment;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;

import cz.uhk.umte_customeventplanner.R;

/**
 * Dialog fragment, which crates a time picker dialog for making user to be able to choose a time in hours and minutes.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private TextView timeInput;

    /**
     * Sets up the current system time of the time picker and returns the dialog to the user.
     *
     * @param savedInstanceState
     * @return date picker dialog
     */
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        timeInput = getActivity().findViewById(R.id.startTimeInput);
        final String timeInputString = timeInput.getText().toString();
        final int hour;
        final int minute;
        if (timeInputString != null && !timeInputString.isEmpty()) {
            final String[] splitString = timeInputString.split(":");
            final String hourString = splitString[0];
            final String minuteString = splitString[1];
            hour = Integer.parseInt(hourString);
            minute = Integer.parseInt(minuteString);
        } else {
            final Calendar calendar = Calendar.getInstance();
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);
        }
        return new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, this, hour, minute, true);
    }

    /**
     * When the dialog is confirmed, sets the time to the input.
     *
     * @param view
     * @param hourOfDay
     * @param minute
     */
    @Override
    public void onTimeSet(final TimePicker view, final int hourOfDay, final int minute) {
        final String time = hourOfDay + ":" + minute;
        timeInput = getActivity().findViewById(R.id.startTimeInput);
        timeInput.setText(time);
    }
}
