package cz.uhk.umte_customeventplanner.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;

import cz.uhk.umte_customeventplanner.R;
import cz.uhk.umte_customeventplanner.model.Event;
import cz.uhk.umte_customeventplanner.utility.time.TimeUtility;
import cz.uhk.umte_customeventplanner.utility.save.object.UserDataSaveUtility;

/**
 * Shows detailed info about a selected event from the list.
 */
public class DetailedEventFragment extends Fragment {

    private Event event;
    private int eventPosition;

    /**
     * Creates the view
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return the created view of the fragment
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detailed_event, container, false);
    }

    /**
     * Takes the arriving event data and fills the text items of the screen with the data.
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull final View view, final @Nullable Bundle savedInstanceState) {
        event = this.getArguments().getParcelable("detailedEvent");
        eventPosition = this.getArguments().getInt("eventPosition");
        fillOutput();
        view.findViewById(R.id.eventBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                NavHostFragment.findNavController(DetailedEventFragment.this).navigate(R.id.action_returnToEventList);
            }
        });
        view.findViewById(R.id.eventEditButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("eventToChange", event);
                bundle.putInt("eventPosition", eventPosition);
                NavHostFragment.findNavController(DetailedEventFragment.this).navigate(R.id.action_toChangeEvent, bundle);
            }
        });
        view.findViewById(R.id.eventRemoveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
                alertDialog.setTitle("Smazání události:");
                alertDialog.setMessage("Opravdu si přejete vymazat událost: " + event.getName() + "?");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setPositiveButton(R.string.Ano, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        final Bundle bundle = new Bundle();
                        bundle.putInt("deleteId", eventPosition);
                        bundle.putBoolean("delete", true);
                        NavHostFragment.findNavController(DetailedEventFragment.this).navigate(R.id.action_returnToEventList, bundle);
                    }
                });
                alertDialog.setNegativeButton(R.string.Ne, null).show();
            }
        });
        view.findViewById(R.id.button_measureEvent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("detailedEvent", event);
                bundle.putInt("eventPosition", eventPosition);
                NavHostFragment.findNavController(DetailedEventFragment.this).navigate(R.id.action_detailedEventFragment_to_eventSensorsFragment, bundle);
            }
        });
        final TextView dateText = view.findViewById(R.id.dateOfEventText);
        dateText.setText("Datum: " + event.getDateOfEvent());
    }

    /**
     * Fills the output by the data from the event.
     */
    private void fillOutput() {
        final TextView nameText = getActivity().findViewById(R.id.eventNameHead);
        nameText.setText(event.getName());
        final TextView descriptionText = getActivity().findViewById(R.id.eventDescriptionText);
        descriptionText.setText("Popis: " + event.getDescription());
        if (event.getLocationFrom() != null && !event.getLocationFrom().isEmpty()) {
            final TextView locationFrom = getActivity().findViewById(R.id.locationFromText);
            locationFrom.setText("Cesta z: " + event.getLocationFrom());
        }
        final TextView location = getActivity().findViewById(R.id.eventLocationText);
        location.setText("Cesta do: " + event.getLocation());
        final TextView startTimeText = getActivity().findViewById(R.id.timeOfStartText);
        final String startTime = event.getTimeOfBeginning();
        startTimeText.setText("Čas začátku: " + startTime);
        final TextView isDailyOutput = getActivity().findViewById(R.id.isDailyOutput);
        if (event.getIsDaily() > 0) {
            isDailyOutput.setText("Je denní.");
        } else {
            isDailyOutput.setText("Není denní.");
        }
        if (event.getLocationFrom() != null && !event.getLocationFrom().isEmpty() && event.getDistanceBetweenLocations() > 0.00f) {
            final UserDataSaveUtility userDataSaveUtility = new UserDataSaveUtility(getActivity().getPreferences(Context.MODE_PRIVATE));
            final double walkingSpeed = userDataSaveUtility.getUserData().getWalkingSpeed();
            if (walkingSpeed > 0.00) {
                try {
                    final String goToAt = TimeUtility.calculateWhenToGo(event, walkingSpeed);
                    final TextView goAtTextView = getActivity().findViewById(R.id.userGoAdviceText);
                    goAtTextView.setText(goToAt);
                } catch (final ParseException e) {
                    Snackbar.make(getView(), "Při určování doporučení vydání se na místo události došlo k chybě.", BaseTransientBottomBar.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }
    }
}