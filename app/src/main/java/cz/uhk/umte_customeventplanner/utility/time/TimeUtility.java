package cz.uhk.umte_customeventplanner.utility.time;

import android.icu.util.Calendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import cz.uhk.umte_customeventplanner.model.Event;

/**
 * Utility abstract class used for calculating the time variables and output.
 */
public abstract class TimeUtility {
    /**
     * Calculates the time between the date of event and current date. Then saves the result in the String, which is then returned as result.
     *
     * @param event An instance of Event, which time parameters are used.
     * @return Either null in case that the event has already happened in the past, or the string containing the result message, which is then goign to be used in the view output.
     * @throws ParseException Exception in case that the parsing of the dateOfEventSimpleDate fails.
     */
    public static String calculateTimeUntilEventHappens(final Event event) throws ParseException {
        final Calendar dateToday = Calendar.getInstance();
        final Date dateOfEventSimpleDate = generateSimpleDateOfEvent(event);
        final long dateOfEventMiliseconds = dateOfEventSimpleDate.getTime();
        final long dateTodayMiliseconds = dateToday.getTimeInMillis();
        long differenceMiniseconds = dateOfEventMiliseconds - dateTodayMiliseconds;
        if (differenceMiniseconds <= 0) {
            return null;
        }
        final long differenceDays = TimeUnit.MILLISECONDS.toDays(differenceMiniseconds);
        long differenceHours = TimeUnit.MILLISECONDS.toHours(differenceMiniseconds);
        long differenceMinutes = TimeUnit.MILLISECONDS.toMinutes(differenceMiniseconds);

        if (differenceHours > 24) {
            while (differenceHours > 24) {
                differenceHours -= 24;
            }
        }
        if (differenceMinutes > 60) {
            while (differenceMinutes > 60) {
                differenceMinutes -= 60;
            }
        }
        return "Začátek za: " + differenceDays + " dní, " + differenceHours + " hodin a " + differenceMinutes + " minut";
    }

    /**
     * Calculates if user can make it up to his event's place in time, which is based on the timeOfTravel. Returns string giving the feedback to the user, if he should move faster or slower, etc.
     *
     * @param event        An instance of event with the required parameters.
     * @param timeOfTravel Current time of travel based on the user's movement speed calculated from the accelerometer sensor in another class.
     * @return Feedback to the user about his movement speed.
     * @throws ParseException Parse exception thrown in case of error of parsing the date from String to Date class.
     */
    public static String calculateIfUserCanMakeItInTime(final Event event, final double timeOfTravel) throws ParseException {
        final Calendar dateToday = Calendar.getInstance();
        final Date dateOfEventSimpleDate = generateSimpleDateOfEvent(event);
        if (dateOfEventSimpleDate == null) {
            return "Během definice dat pro určení stihnutí události došlo k chybě.";
        }
        final long dateOfEventMiliseconds = dateOfEventSimpleDate.getTime();
        final long dateTodayMiliseconds = dateToday.getTimeInMillis();
        //If the event has already happened in the day, tell user that he can´t go to the event in time today.
        if (event.getIsDaily() > 0 && dateTodayMiliseconds > dateOfEventMiliseconds) {
            return "Tuto událost dnes nestihnete.";
        }
        //If the event has already happened in past, tell user that he can´t go there in time.
        if (event.getIsDaily() < 1 && dateTodayMiliseconds > dateOfEventMiliseconds) {
            return "Tuto událost už nestihnete.";
        }
        final long timeOfTravelInMiliseconds = TimeUnit.SECONDS.toMillis((long) timeOfTravel);
        final long result = dateTodayMiliseconds + timeOfTravelInMiliseconds;
        if (result < dateOfEventMiliseconds) {
            System.out.println("Výpočet.: " + result);
            return "Jdete příliš moc rychle.";
        }
        if (result == dateOfEventMiliseconds) {
            return "Jdete dostatečně rychle.";
        }
        if (result > dateOfEventMiliseconds) {
            return "Jdete příliš pomalu!";
        }
        return "Z důvodu chyby nelze zjistit, zda stihnete dojít včas na dané místo.";
    }

    public static String calculateWhenToGo(final Event event, final double speed) throws ParseException {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy;HH:mm");
        final Date dateOfEventSimpleDate = simpleDateFormat.parse(event.getDateOfEvent() + ";" + event.getTimeOfBeginning());
        final long dateOfEventMiliseconds = dateOfEventSimpleDate.getTime();
        final Calendar dateToday = Calendar.getInstance();
        final long dateTodayMiliseconds = dateToday.getTimeInMillis();
        if (event.getIsDaily() < 1 && dateTodayMiliseconds > dateOfEventMiliseconds) {
            return "Tuto událost už nestihnete. Pro doporučení, kdy vyrazit, prosím, změňte tuto událost na denní, nebo upravte datum události.";
        }
        final double timeOfJourney = event.getDistanceBetweenLocations() / speed;
        final long timeOfJourneyMiliseconds = TimeUnit.SECONDS.toMillis(Math.round(timeOfJourney));
        final long timeToGo = dateOfEventMiliseconds - timeOfJourneyMiliseconds;
        long hoursToGo = TimeUnit.MILLISECONDS.toHours(timeToGo) + 1;
        long minutesToGo = TimeUnit.MILLISECONDS.toMinutes(timeToGo);

        if (hoursToGo > 24) {
            while (hoursToGo > 24) {
                hoursToGo -= 24;
            }
        }
        if (minutesToGo > 60) {
            while (minutesToGo > 60) {
                minutesToGo -= 60;
            }
        }
        System.out.println("Vyražte nejpozději v.: " + hoursToGo + ":" + minutesToGo);
        return "Vyražte nejpozději v.: " + hoursToGo + ":" + minutesToGo;

    }

    /**
     * Generates date of event in Date instance.
     *
     * @param event The event which date is going to be taken.
     * @return Date instance with either the exact date of the event, or if the event is daily, date with the today's date and time of the event.
     */
    private static Date generateSimpleDateOfEvent(final Event event) {
        final Calendar dateToday = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy;HH:mm");
        //If the event happens only in one day, get the required information from the event instance.
        if (event.getIsDaily() < 1) {
            try {
                return simpleDateFormat.parse(event.getDateOfEvent() + ";" + event.getTimeOfBeginning());
            } catch (final ParseException e) {
                e.printStackTrace();
                return null;
            }
            //If the event happens daily, make date of event as today's date, but with the time of the event.
        } else {
            byte month = (byte) (dateToday.get(Calendar.MONTH) + 1);
            while (month > 12) {
                month = (byte) (month - 12);
            }
            final String dateTodayString = dateToday.get(Calendar.DAY_OF_MONTH) + "." + month + "." + dateToday.get(Calendar.YEAR);
            try {
                return simpleDateFormat.parse(dateTodayString + ";" + event.getTimeOfBeginning());
            } catch (final ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
