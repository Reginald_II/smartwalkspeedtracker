package cz.uhk.umte_customeventplanner.utility.save.list;

import java.util.List;

/**
 * Utility interface used for saving lists in Json format.
 *
 * @param <T> Parametrised class, which is going to be saved, restored or changed.
 */
public interface ISaveUtility<T> {

    /**
     * Adds a new instance to the list. Then saves the list.
     *
     * @param t The instance which is going to be added and saved in the list.
     * @return Changed saved list.
     */
    public List<T> add(final T t);

    /**
     * Changes an actual instance in the list. Then saves the list.
     *
     * @param t The instance which is going to be changed.
     * @param i The index of the instance in the list.
     * @return The updated list.
     */
    public List<T> change(final T t, final int i);

    /**
     * Removes an instance from the list.
     *
     * @param i Index of the instance
     * @return Updated list.
     */
    public List<T> remove(final int i);
}
