package cz.uhk.umte_customeventplanner.utility.save.list;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.umte_customeventplanner.model.Event;

/**
 * Utility for working with the data of events.
 */
public class EventSaveUtility implements ISaveUtility<Event> {
    private static List<Event> events;
    private final SharedPreferences preferences;

    public EventSaveUtility(final SharedPreferences preferences) {
        this.preferences = preferences;
        if (events == null) {
            events = restore();
        }
    }

    @Override
    public List<Event> add(final Event event) {
        events.add(event);
        save();
        return events;
    }

    @Override
    public List<Event> change(final Event event, final int i) {
        events.set(i, event);
        save();
        return events;
    }

    @Override
    public List<Event> remove(int i) {
        events.remove(i);
        save();
        return events;
    }

    /**
     * Saves events in the device as Json file.
     */
    private void save() {
        final Gson gson = new Gson();
        final String eventsJson = gson.toJson(events);
        preferences.edit().putString("savedEvents", eventsJson).commit();
    }

    /**
     * Restores events from the device's Json file, if it exists in the device.
     *
     * @return Either restored list of events or if there are no events saved, creates and returns new Array list.
     */
    private List<Event> restore() {
        final Gson gson = new Gson();
        final String eventsJson = preferences.getString("savedEvents", "");
        final Type type = new TypeToken<List<Event>>() {
        }.getType();
        if (!eventsJson.isEmpty()) {
            return gson.fromJson(eventsJson, type);
        }
        return new ArrayList<>();
    }

    public List<Event> getEvents() {
        return events;
    }
}
