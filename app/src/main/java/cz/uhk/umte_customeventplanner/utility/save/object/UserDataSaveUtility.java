package cz.uhk.umte_customeventplanner.utility.save.object;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import cz.uhk.umte_customeventplanner.model.UserData;

public class UserDataSaveUtility {

    private static UserData userData;
    private final SharedPreferences preferences;

    public UserDataSaveUtility(final SharedPreferences preferences) {
        this.preferences = preferences;
        if (userData == null) {
            userData = restore();
        }
    }

    public UserData getUserData() {
        return userData;
    }

    /**
     * Sets the new UserData instance and saves it on the device.
     *
     * @param userData
     */
    public void setUserData(final UserData userData) {
        this.userData = userData;
        save();
    }

    private void save() {
        final Gson gson = new Gson();
        final String eventsJson = gson.toJson(userData);
        preferences.edit().putString("savedUserData", eventsJson).commit();
        System.out.println("Uživatelská data byla úspěšně uložena s rychlostí.: " + getUserData().getWalkingSpeed());
    }

    /**
     * Restores events from the device's Json file, if it exists in the device.
     *
     * @return Either restored list of events or if there are no events saved, creates and returns new Array list.
     */
    private UserData restore() {
        final Gson gson = new Gson();
        final String eventsJson = preferences.getString("savedUserData", "");
        final Type type = new TypeToken<UserData>() {
        }.getType();
        if (!eventsJson.isEmpty()) {
            return gson.fromJson(eventsJson, type);
        }
        //In case of non existing UserData creates new instance, with the default walking speed 0;
        return new UserData(0.00);
    }
}
